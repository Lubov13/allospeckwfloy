﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace allo.POM
{
     public class CreditPage
    {
        private IWebDriver _driver;
        public By TitleCredit = By.XPath("/html/body/div[1]/div/div/div[2]/ul/li[2]/span");

        public CreditPage(IWebDriver driver)
        {
            this._driver = driver;

        }

        public IWebElement FindElementCreditMainLabel()
        {
            return _driver.FindElement(TitleCredit);

        }
        public string GetTextCredit()
        {
            return FindElementCreditMainLabel().Text;
        }

    }
}
