﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace allo.POM
{
    public class LoginForm
    {
        private IWebDriver _driver;

        public By FieldEmail = By.XPath("/html/body/div[3]/div/div/div[2]/div/div/div[1]/div[1]/form/div/div[1]/div[1]/div[1]/input");
        public By ButtonLogin = By.XPath("/html/body/div[3]/div/div/div[2]/div/div/div[1]/div[1]/form/div/button");
        public By FieldPassword = By.XPath("/html/body/div[3]/div/div/div[2]/div/div/div[1]/div[1]/form/div/div[1]/div[2]/div[1]/input");

        public LoginForm(IWebDriver driver)
        {
            this._driver = driver;

        }
        public LoginForm EnterFieldEmail()
        {
            _driver.FindElement(FieldEmail).SendKeys("qarpova@gmail.com");
            return this;
        }
        public LoginForm EnterFieldPassword()
        {
            _driver.FindElement(FieldPassword).SendKeys("veterinar13");
            return this;
        }
        public MainPage ClickLoginButton()
        {
            _driver.FindElement(ButtonLogin).Click();
            return new MainPage(_driver);

        }

    }
}
