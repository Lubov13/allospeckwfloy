﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace allo.POM
{
    public class FeedbackFormPage
    {
        private IWebDriver _driver;

        public By FieldEmail = By.XPath("/html/body/div[1]/div/div/div[2]/div/div/div/div/div/div[2]/div/form/div[2]/div/div[2]/div/input");
        public By ButtonLogin = By.XPath("/html/body/div[1]/div/div/div[2]/div/div/div/div/div/div[2]/div/form/button");
        public By FieldName = By.XPath("/html/body/div[1]/div/div/div[2]/div/div/div/div/div/div[2]/div/form/div[1]/div/div/input");
        public By FieldMessage = By.XPath("/html/body/div[1]/div/div/div[2]/div/div/div/div/div/div[2]/div/form/div[4]/div/div/textarea");

        public FeedbackFormPage(IWebDriver driver)
        {
            this._driver = driver;

        }
        public FeedbackFormPage EnterFieldName()
        {
            _driver.FindElement(FieldName).SendKeys("Любовь");
            return this;
        }
        public FeedbackFormPage EnterFieldEmail()
        {
            _driver.FindElement(FieldEmail).SendKeys("qarova@gmail.com");
            return this;
        }
        public FeedbackFormPage EnterFieldMessage()
        {
            _driver.FindElement(FieldMessage).SendKeys("Привет!");
            return this;
        }
        
        public MainPage ClickLoginButton()
        {
            _driver.FindElement(ButtonLogin).Click();
            return new MainPage(_driver);

        }
    }
}
