﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace allo.POM
{
   public class MainPage
    {
        private IWebDriver _driver;
        public By Blog = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[1]/a");
        public By Fishka = By.XPath("/ html / body / div[1] / div / div / div[1] / div[1] / div[1] / div / div[3] / ul / li[2] / a");
        public By Vakansii = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[3]/a");
        public By Shops = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[4]/a");
        public By DeliveryAndPayment = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[5]/a");
        public By Credit = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[6]/a");
        public By WarrantyAndService = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[7]/a");
        public By Contacts = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[8]/a");
        public By SwitcherLanguageToogle = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[4]/span[2]/div");
        public By SwitcherDarkLightToogle = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[4]/span[2]/div");
        public By Geolocation = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[1]/div");
        public By LiveSmart = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[2]/div/ul/li[1]/a/p");
        public By AlloCash = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[2]/div/ul/li[2]/a/p");
        public By AlloUpgrade = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[2]/div/ul/li[3]/a/p");
        public By AlloChange = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[2]/div/ul/li[4]/a/p");
        public By Markdown = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[2]/div/ul/li[5]/a/p");
        public By Discounts = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[2]/div/ul/li[5]/a/p");
        public By BurgerMenuCatalog = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[3]/div/div");
        public By HeaderPhone = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[3]/div/ul/li[2]/div/div[1]/span");
        public By SearchingInput = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[3]/div/ul/li[4]/div/div/form/label/input");
        public By SearchingSubmitButton = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[3]/div/ul/li[4]/div/div/form/button/svg");
        public By ButtonLogin = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[3]/div/ul/li[5]/div/div/button[1]");
        public By ButtonRegistration = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[3]/div/ul/li[5]/div/div/button[2]");
        public By ButtonCart = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[3]/div/ul/li[6]/div/div[1]");
        public By MainLabelAllo = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[2]/div/a/img[1]");
        public By UserAuthorizedLabel = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[3]/div/ul/li[5]/div/div/div[1]/span");
        public By ContactsSent = By.XPath("/html/body/div[1]/div/div/div[2]/div/div/div/div/div/div[2]/div/div");

        public MainPage(IWebDriver driver)
        {
            this._driver = driver;

        }
        public MainPage Clickcart()
        {
            _driver.FindElement(ButtonCart).Click();
            return this;

        }
        public AlloObminPage ClickAlloObminButton()
        {
            _driver.FindElement(AlloChange).Click();
            return new AlloObminPage(_driver);

        }
        public DiscountPage ClickDiscountButton()
        {
            _driver.FindElement(Discounts).Click();
            return new DiscountPage (_driver);

        }
        public LoginForm ClickLoginButton()
        {
            _driver.FindElement(ButtonLogin).Click();
            return new LoginForm(_driver);

        }
        public IWebElement FindElementUserAuthorizedLabel()
        {
            return _driver.FindElement(UserAuthorizedLabel);

        }
        public string GetTextUserAuthorizedLabel()
        {
            return FindElementUserAuthorizedLabel().Text;
        }
        
            public SearchPage EnterFieldSearch()
        {
            _driver.FindElement(SearchingInput).SendKeys("утюг\n");
            return new SearchPage(_driver);
        }
        public FishkaPage ClickFishkaButton()
        {
            _driver.FindElement(Fishka).Click();
            return new FishkaPage(_driver);

        }
        public ContactsPage ClickContactsButton()
        {
            _driver.FindElement(Contacts).Click();
            return new ContactsPage(_driver);

        }
        public IWebElement FindElementFeedbackFormPageLabel()
        {
            return _driver.FindElement(ContactsSent);

        }
        public string GetTextFeedbackFormPageLabel()
        {
            return FindElementFeedbackFormPageLabel().Text;
        }
        public CreditPage ClickCreditButton()
        {
            _driver.FindElement(Credit).Click();
            return new CreditPage(_driver);

        }
       
        public SearchArtPage EnterFieldSearchArt()
        {
            _driver.FindElement(SearchingInput).SendKeys("471158\n");
            return new SearchArtPage(_driver);
        }


    }
}
