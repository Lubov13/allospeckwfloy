﻿using allo.POM;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using TechTalk.SpecFlow;

namespace allo.Steps
{

    [Binding]
    public class SearchSteps
    {
        public IWebDriver driver;
        public MainPage mainPage;
        public SearchPage searchPage;
        public SearchArtPage searchArtPage;


        [BeforeScenario]
        public void CreateDriver()
        {
            driver = new ChromeDriver(@"C:\driver");
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromMinutes(5);

        }
        [Given(@"Allo website is open")]
        public void GivenAlloWebsiteIsOpen()
        {
            driver.Navigate().GoToUrl("https://allo.ua");
            driver.Manage().Window.Maximize();
            mainPage = new MainPage(driver);
            searchPage = new SearchPage(driver);
            searchArtPage = new SearchArtPage(driver);

        }
        [When(@"User enter to search field ""(.*)""")]
        public void WhenUserEnterToSearchField(string p0)
        {
            mainPage.EnterFieldSearch();
        }

        [Then(@"User found the right item")]
        public void ThenUserFoundTheRightItem()
        {
            string mainLabelSearch = searchPage.GetTextSearchLabel();
            Assert.AreEqual("Праски", mainLabelSearch);
        }

        [When(@"User enter to search field article ""(.*)""")]
        public void WhenUserEnterToSearchFieldArticle(int p0)
        {
           mainPage.EnterFieldSearchArt();
        }

        [Then(@"User found the right item")]
        public void ThenUserFoundTheRightItemArt()
        {
            string mainLabelSearchArt = searchArtPage.GetTextSearchArtLabel();
            Assert.AreEqual("471158", mainLabelSearchArt);
        }
    }

}
