﻿using allo.POM;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using TechTalk.SpecFlow;

namespace allo.Steps
{
    [Binding]
    public class HeaderSteps
    {
        public IWebDriver driver;
        public MainPage mainPage;
        public AlloObminPage alloObminPage;
        public DiscountPage discountPage;
        public ContactsPage contactsPage;

        //[BeforeScenario]
        //public void CreateDriver()
        //{
        //    driver = new ChromeDriver(@"C:\driver");
        //    driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromMinutes(5);

        //}
        //[Given(@"Allo website is open")]
        //public void GivenAlloWebsiteIsOpen()
        //{
        //    driver.Navigate().GoToUrl("https://allo.ua");
        //    driver.Manage().Window.Maximize();
        //    mainPage = new MainPage(driver);
        //    alloObminPage = new AlloObminPage(driver);
        //    discountPage = new DiscountPage(driver);
        //    contactsPage = new ContactsPage(driver);

        //}
        [When(@"User clicks on AlloObmin button")]
        public void WhenUserClicksOnAlloObminButton()
        {
            mainPage.ClickAlloObminButton();
        }
        
        [Then(@"Page AlloObmin open")]
        public void ThenPageAlloObminOpen()
        {
           string titleAlloObmin= alloObminPage.GetTextAlloObmin();
            Assert.AreEqual("Поміняй свій старий телефон на новий!", titleAlloObmin);

        }
        [When(@"User clicks on discount  button")]
        public void WhenUserClicksOnDiscountButton()
        {
            mainPage.ClickDiscountButton();
        }

        [Then(@"Page Discount  open")]
        public void ThenPageDiscountOpen()
        {
            string titleDiscount = discountPage.GetTextDiscount();
            Assert.AreEqual("РОЗПРОДАЖ ТОВАРІВ З УЦІНКОЮ", titleDiscount);

        }
        [When(@"User clicks on contacts  button")]
        public void WhenUserClicksOnContactsButton()
        {
            mainPage.ClickContactsButton();
        }

        [Then(@"Page Contacts  open")]
        public void ThenPageContactsOpen()
        {
            string titleContacts = contactsPage.GetTextContacts();
            Assert.AreEqual("Контакти інтернет-магазин АЛЛО", titleContacts);
        }


    }
}
