﻿using allo.POM;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using TechTalk.SpecFlow;

namespace allo.Steps
{
    [Binding]
    public class CartSteps
    {
        public IWebDriver driver;
        public MainPage mainPage;
        public CartPopUp cartPopUp;
        //[BeforeScenario]
        //public void CreateDriver()
        //{
        //    driver = new ChromeDriver(@"C:\driver");
        //    driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromMinutes(5);

        //}
        //[Given(@"Allo website is open")]
        //public void GivenAlloWebsiteIsOpen()
        //{
        //    driver.Navigate().GoToUrl("https://allo.ua");
        //    driver.Manage().Window.Maximize();
        //    mainPage = new MainPage(driver);
        //    cartPopUp = new CartPopUp(driver);
        //}
        
        [When(@"User clicks on cart")]
        public void WhenUserClicksOnCart()
        {
            mainPage.Clickcart();
        }
        
        [Then(@"Cart is empty")]
        public void ThenCartIsEmpty()
        {
            string CartIsEmpty = cartPopUp.GetTextCart();
            Assert.AreEqual("Ваш кошик порожній.", CartIsEmpty);
        }
    }
}
