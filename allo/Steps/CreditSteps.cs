﻿using allo.POM;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using TechTalk.SpecFlow;

namespace allo.Steps
{
    [Binding]
    public class CreditSteps
    {
        public IWebDriver driver;
        public MainPage mainPage;
        public CreditPage creditPage;

        [BeforeScenario]
        public void CreateDriver()
        {
            driver = new ChromeDriver(@"C:\driver");
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromMinutes(5);

        }
        [Given(@"Allo website is open")]
        public void GivenAlloWebsiteIsOpen()
        {
            driver.Navigate().GoToUrl("https://allo.ua");
            driver.Manage().Window.Maximize();
            mainPage = new MainPage(driver);
            creditPage = new CreditPage(driver);
        }
        [When(@"User clicks on credit  button")]
        public void WhenUserClicksOnCreditButton()
        {
            mainPage.ClickCreditButton();
        }
        
        [Then(@"Page Credit  open")]
        public void ThenPageCreditOpen()
        {
            string TitleCredit = creditPage.GetTextCredit();
            Assert.AreEqual("Кредит", TitleCredit);
        }
    }
}
