﻿Feature: Search
As a user
	I want to have access to the search
	In order to to find the right item

Scenario: Find the right item
	Given  Allo website is open
	When User enter to search field "утюг"
	Then User found the right item

Scenario: Product search by article
	Given Allo website is open
	When User enter to search field article "471158"
	Then User found the right item

